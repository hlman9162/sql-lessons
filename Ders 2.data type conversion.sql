--İmplicit Conversion: Oracle ifadədə müxtəlif məlumat növləri istifadə edildikdə avtomatik olaraq çevirə bilər. Məsələn, tam --ədədlər sütununu sətirlə birləşdirərək tam ədədi sətirə avtomatik çevirmək üçün Oracle tərəfindən edilə bilər.

SELECT emp_id || '-' || emp_name FROM employees;

--Excplicit: CAST və ya TO_* funksiyaları məlumat növlərini açıq şəkildə çevirmək üçün istifadə edilə bilər.

CAST() :
SELECT CAST(emp_id AS VARCHAR2(10)) FROM employees;

CAST funksiyası bir çox müxtəlif məlumat növünün çevrilməsi üçün istifadə edilə bilər. Misal üçün:

Number tipli datanin metn tipine çevrilməsi (VARCHAR2),
Mətn rəqəmə çevrilir (NUMBER),
Müxtəlif formatlarda tarix/tarix vaxtı məlumatlarını göstərmək üçün istifadə edilə bilər.

TO_CHAR() :
SELECT TO_CHAR(emp_id) FROM employees;

TO_NUMBER() :
SELECT TO_NUMBER(emp_salary) FROM employees;

TO_DATE() :
SELECT TO_DATE('2024-05-06', 'YYYY-MM-DD') FROM dual;
