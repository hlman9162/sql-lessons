Data Control Language (DCL)
Oracle SQL-də verilənlər bazasında "yetkilendirme" əməliyyatlarını idarə etmək üçün DCL (Data Control Language) əmrləri istifadə olunur. Bu əmrlər istifadəçilərin müəyyən obyektlər üzərində hansı əməliyyatları yerinə yetirə biləcəyini müəyyən edir. Ən çox yayılmış iki DCL əmri GRANT və REVOKE-dir. 




***GRANT əmri istifadəçilərə və ya rollara müəyyən icazələr vermək üçün istifadə olunur. Təqdim edilmiş istifadəçilər və ya rollar verilənlər bazası obyekti üzərində müəyyən əməliyyatları yerinə yetirmək üçün icazə alırlar (cədvəl, görünüş, prosedur və s.).

GRANT privilege [, privilege ] ...
ON object
TO {user | role | PUBLIC} [, {user | role | PUBLIC}] ...
[WITH GRANT OPTION];

Privilege: Verilecek selahiyyetlerin tipini teyin edir(meselen, SELECT, INSERT, UPDATE, DELETE).
Object: Selahiyyet ve ya icaze verilecek obyekti teyin edir(meselen,cedvel adı).
User: Selahiyyet verilecek istifadeci.
Role: Selahiyyet verilecek rol.
PUBLIC: Butun istifadecilere selahiyyet vermek ucun istifade edilir.
WITH GRANT OPTION: Istifadeciye selahiyyetleri basqa istifadecileri vermek icazasini temin edir.



1.Cədvəldə SELECT səlahiyyətinin verilməsi:

GRANT SELECT ON employees TO john;



2.DML emirlerine selahiyyetler vermek:

GRANT SELECT, INSERT, UPDATE, DELETE ON employees TO john;



3.Cədvəldə SELECT səlahiyyətinin verilməsi və bu səlahiyyəti başqalarına vermək icazəsi:

GRANT SELECT ON employees TO john WITH GRANT OPTION;



4.Cədvəldəki bütün istifadəçilərə (PUBLIC) SEÇİM icazəsinin verilməsi

GRANT SELECT ON employees TO PUBLIC;
***





***REVOKE əmri istifadəçilərdən və ya rollardan əvvəllər verilmiş icazələri ləğv etmək üçün istifadə olunur.

REVOKE privilege [, privilege ] ...
ON object
FROM {user | role | PUBLIC} [, {user | role | PUBLIC}] ...;

Privilege:Geri alinacaq selahiyyetleri teyin edir(meselen, SELECT, INSERT, UPDATE, DELETE).
Object: Selahiyyetin geri alinacagi obyekti gosterir (meselen, cedvel adı).
User: Selahiyyeti geri alinacaq istifadeci
Role: Selahiyyeti geri alinacaq rol
PUBLIC: Butun istifadecilerden selahiyyeti geri almaq ucun istifade edilir.





1.Bir istifadeciden selahiyyetinin geri alinmasi

REVOKE SELECT ON employees FROM john;



2.Bir istifadeciden DML selahiyyetlerini geri almaq

REVOKE SELECT, INSERT, UPDATE, DELETE ON employees FROM john;



3.Roldan səlahiyyəti ləğv etmək üçün:

REVOKE SELECT ON employees FROM hr_role;



4.Cədvəldəki SELECT imtiyazının bütün istifadəçilərdən ləğv edilməsi (PUBLIC)

REVOKE SELECT ON employees FROM PUBLIC;



Əlavə informasiya


Səlahiyyətlər istifadəçi və ya rol müəyyən edildikdən sonra tətbiq edilir. Səlahiyyət verildikdən sonra səlahiyyətli istifadəçi və ya rol müəyyən edilmiş obyektdə müəyyən əməliyyatları yerinə yetirə bilər.
İLƏ GRANT OPSİYONUNDAN istifadə ehtiyatla aparılmalıdır, çünki bu seçim istifadəçiyə başqalarına imtiyazlar verməyə imkan verir və səlahiyyət zəncirləri yarada bilər.
İcazələr ləğv edildikdə, GRANT OPTION İLƏ verilmiş icazələr silsiləsindəki bütün istifadəçilər də icazələrini itirirlər.
Oracle SQL-də DCL əmrləri ilə istifadəçi imtiyazlarını idarə etmək verilənlər bazası təhlükəsizliyini təmin etmək üçün çox vacibdir. Bu əmrlər sayəsində hansı istifadəçilərin hansı əməliyyatları yerinə yetirə biləcəyinə ətraflı nəzarət etmək mümkündür.







