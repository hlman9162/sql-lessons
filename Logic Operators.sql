Logic Operators (Mentiq operatorlari)

**AND OPERATORU

--AND operatoru iki və ya daha çox şərtin hamısı doğru olduqda istifadə olunur.

SELECT * FROM employees
WHERE department_id = 10 AND salary > 5000;




**OR OPERATORU

--OR operatoru iki və ya daha çox şərtdən hər hansı birinin doğru olması lazım olduqda istifadə olunur.

SELECT * FROM employees
WHERE department_id = 10 OR salary > 5000;




**NOT OPERATORU

--Şərti tərsinə çevirmək üçün NOT operatorundan istifadə olunur.

SELECT * FROM employees
WHERE NOT department_id = 10;




**IN OPERATORU

--IN operatoru dəyərin hər hansı bir dəyər dəstinə uyğun olub olmadığını yoxlayır.

SELECT * FROM employees
WHERE department_id IN (10, 20, 30);




**BETWEEN OPERATORU

--BETWEEN operatoru dəyərin müəyyən diapazonda olub olmadığını yoxlayır.

SELECT * FROM employees
WHERE salary BETWEEN 3000 AND 5000;





**LIKE OPERATORU

--LIKE operatoru simvollar sətirinin müəyyən nümunəyə uyğun olub olmadığını yoxlayır. 
--% və _ joker simvollarından istifadə olunur.

SELECT * FROM employees
WHERE first_name LIKE 'J%';




**IS NULL OPERATORU

--IS NULL operatoru sütunda NULL dəyərinin olub olmadığını yoxlayır.

SELECT * FROM employees
WHERE commission_pct IS NULL;




**IS NOT NULL OPERATORU

--IS NOT NULL operatoru sütunda NULL dəyərlərinin olmadığını yoxlayır.

SELECT * FROM employees
WHERE commission_pct IS NOT NULL;



**EXISTS OPERATORU

--EXISTS operatoru daxili sorğunun bir və ya bir neçə sətir qaytarıb-qaytarmadığını yoxlayır

SELECT * FROM departments d
WHERE EXISTS (SELECT 1 FROM employees e WHERE e.department_id = d.department_id);




**NETICE:

AND: Bütün şərtlər doğru olmalıdır.
OR: Şərtlərdən hər hansı birinin doğru olması kifayətdir.
NOT: Sertin tersini alir.
IN: Dəyərin verilmiş dəyər dəstindən hər hansı birinə uyğun olub olmadığını yoxlayır.
BETWEEN: Dəyərin müəyyən diapazonda olub olmadığını yoxlayır.
LİKE: Simvollar sətirinin müəyyən nümunəyə uyğun olub olmadığını yoxlayır.
IS NULL: Sütunun NULL dəyərinin olub olmadığını yoxlayır.
IS NOT NULL: Sütunun NULL dəyərlərinin olmadığını yoxlayır.
EXISTS: İç-içə sorğunun bir və ya daha çox sətir qaytarıb-qaytarmadığını yoxlayır.


