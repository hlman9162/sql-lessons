--Oracle SQL-də verilənlər bazası cədvəllərində verilənlərin tamlığını və dəqiqliyini təmin etmək üçün
--məhdudiyyətlərdən istifadə olunur. 
--Məhdudiyyətlər cədvəllərin sütunlarında hansı növ məlumatların saxlanıla biləcəyinə nəzarət edir. 
--Budur Oracle SQL-də əsas məhdudiyyətlər və hər birinin ətraflı izahı:

1.Primary Key Constraint

--Cədvəldəki hər bir sıranı unikal şəkildə müəyyən edən bir və ya bir neçə sütunu təyin edir.
--PRIMARY KEY məhdudiyyətləri olan sütunlarda NULL dəyərlər ola bilməz.

CREATE TABLE employees (
  employee_id NUMBER PRIMARY KEY,
  first_name VARCHAR2(50),
  last_name VARCHAR2(50)
);


2.Foreign Key Constraint

--Bir cədvəldəki sütunlara digər cədvəldəki sütunlara istinad etməyə imkan verir.
--Məlumatların bütövlüyünü qoruyur və verilənlər bazasında əlaqələri müəyyənləşdirir.

CREATE TABLE departments (
  department_id NUMBER PRIMARY KEY,
  department_name VARCHAR2(50)
);

CREATE TABLE employees (
  employee_id NUMBER PRIMARY KEY,
  first_name VARCHAR2(50),
  last_name VARCHAR2(50),
  department_id NUMBER,
  CONSTRAINT fk_department FOREIGN KEY (department_id)
    REFERENCES departments (department_id)
);


3.Unique Key Constraints

--Sütun və ya sütunlar birləşməsindəki bütün dəyərlərin unikal olmasını təmin edir.
--Cədvəldə birdən çox UNİKAL məhdudiyyət ola bilər.

CREATE TABLE employees (
  employee_id NUMBER PRIMARY KEY,
  email VARCHAR2(100) UNIQUE,
  first_name VARCHAR2(50),
  last_name VARCHAR2(50)
);


4.NOT NULL Constraints

--Sütundakı bütün dəyərlərin NULL olmadığını təmin edir.

CREATE TABLE employees (
  employee_id NUMBER PRIMARY KEY,
  first_name VARCHAR2(50) NOT NULL,
  last_name VARCHAR2(50) NOT NULL
);


5.CHECK Constraints

--Sütunun müəyyən bir şərtə əsaslanaraq qəbul edə biləcəyi dəyərləri məhdudlaşdırır.

CREATE TABLE employees (
  employee_id NUMBER PRIMARY KEY,
  salary NUMBER,
  CHECK (salary >= 0)
);


6.** Məhdudiyyətlərin İdarə Edilməsi

--Məhdudiyyətlərin əlavə edilməsi:
--Məhdudiyyətlər cədvəl yaradılarkən və ya cədvəl yaradıldıqdan sonra əlavə edilə bilər.

CREATE TABLE employees (
  employee_id NUMBER PRIMARY KEY,
  first_name VARCHAR2(50) NOT NULL,
  last_name VARCHAR2(50) NOT NULL,
  email VARCHAR2(100) UNIQUE,
  department_id NUMBER,
  salary NUMBER CHECK (salary >= 0),
  CONSTRAINT fk_department FOREIGN KEY (department_id) REFERENCES departments (department_id)
);


**Table yaratdiqdan sonra:

ALTER TABLE employees
ADD CONSTRAINT unique_email UNIQUE (email);

ALTER TABLE employees
ADD CONSTRAINT check_salary CHECK (salary >= 0);


**Məhdudiyyətlərin aradan qaldırılması:
Cədvəl yaradıldıqdan sonra məhdudiyyətlər aradan qaldırıla bilər.

ALTER TABLE employees
DROP CONSTRAINT unique_email;

ALTER TABLE employees
DROP CONSTRAINT check_salary;


**Məhdudiyyətlərin adlandirilmasi
Məhdudiyyətlər yaradılarkən və bu adlardan istifadə edərək idarə edildikdə adlandırıla bilər. Fərdi adlardan istifadə məhdudiyyətləri idarə etməyi asanlaşdırır.


CREATE TABLE employees (
  employee_id NUMBER PRIMARY KEY,
  email VARCHAR2(100),
  CONSTRAINT unique_email UNIQUE (email),
  salary NUMBER,
  CONSTRAINT check_salary CHECK (salary >= 0)
);


**Mehdudiyyetlerin goruntulemesi

--USER_CONSTRAINTS və USER_CONS_COLUMNS sistem görünüşləri Oracle SQL-də mövcud məhdudiyyətlərə baxmaq üçün istifadə edilə bilər.

SELECT constraint_name, constraint_type, table_name
FROM user_constraints
WHERE table_name = 'EMPLOYEES';

**Netice:

PRIMARY KEY: Cədvəldəki sətirləri unikal şəkildə müəyyənləşdirir.
FOREIGN KEY: Bir cədvəldəki sütunları digər cədvəldəki sütunlara istinad kimi müəyyən edir.
UNIQUE: Sütun və ya sütunlar birləşməsindəki dəyərlərin unikal olmasını təmin edir.
NOT NULL: Sütundakı dəyərlərin NULL olmasının qarşısını alır.
CHECK: Sütunun müəyyən bir şərt əsasında qəbul edə biləcəyi dəyərləri məhdudlaşdırır.


*********************************************** ON DELETE SET NULL **************************************************************

Bu nümunədə, bir şöbə silindikdə, həmin şöbəyə aid işçilərin dept_id sahəsinin NULL olaraq təyin olunmasını təmin edəcəyik.


CREATE TABLE employees_set_null (
    emp_id NUMBER PRIMARY KEY,
    emp_name VARCHAR2(50),
    dept_id NUMBER,
    CONSTRAINT fk_dept_set_null FOREIGN KEY (dept_id) REFERENCES departments(dept_id) ON DELETE SET NULL
);

-- DATA ELAVE ETME
INSERT INTO departments (dept_id, dept_name) VALUES (1, 'Sales');
INSERT INTO departments (dept_id, dept_name) VALUES (2, 'Engineering');

INSERT INTO employees_set_null (emp_id, emp_name, dept_id) VALUES (1, 'Alice', 1);
INSERT INTO employees_set_null (emp_id, emp_name, dept_id) VALUES (2, 'Bob', 2);

-- YOXLAYIRIQ
SELECT * FROM employees_set_null;

-- HER HANSI BIR DEPARTMENT SILME
DELETE FROM departments WHERE dept_id = 1;

-- YENE EMPLOYEE CEDVELINI YOXLAYIRIQ
SELECT * FROM employees_set_null;



*************************************************** ON DELETE CASCADE *************************************************************


Bu nümunədə, bir şöbə silindikdə, həmin şöbəyə aid işçilərin də silinməsini təmin edəcəyik.

CREATE TABLE employees_cascade (
    emp_id NUMBER PRIMARY KEY,
    emp_name VARCHAR2(50),
    dept_id NUMBER,
    CONSTRAINT fk_dept_cascade FOREIGN KEY (dept_id) REFERENCES departments(dept_id) ON DELETE CASCADE
);

-- Melumat elave etmek
INSERT INTO departments (dept_id, dept_name) VALUES (3, 'HR');
INSERT INTO departments (dept_id, dept_name) VALUES (4, 'Marketing');

INSERT INTO employees_cascade (emp_id, emp_name, dept_id) VALUES (3, 'Charlie', 3);
INSERT INTO employees_cascade (emp_id, emp_name, dept_id) VALUES (4, 'Dave', 4);

-- yoxlayiriq
SELECT * FROM employees_cascade;

-- her hansi bir departmeni silirik
DELETE FROM departments WHERE dept_id = 3;

-- yeniden yoxlayiriq
SELECT * FROM employees_cascade;



**Netice:
--employees_set_null cədvəlində, dept_id 1 olan şöbə silindikdə,
--bu şöbəyə hesabat verən Alice əməkdaşının dept_id sahəsi NULL olacaq.
--dept_id 3 olan şöbə employees_cascade cədvəlində silindikdə, bu şöbə ilə əlaqəli olan işçi Çarli də silinəcək.










