Oracle SQL-də tək sətirli funksiyalar bir sıra verilənlər üzərində işləmək üçün istifadə olunur 
və hər biri fərqli məqsədə xidmət edir. 
Göstərilən funksiyaların təsviri və istifadə nümunələri aşağıda verilmişdir:

1.NVL() 

NVL funksiyası dəyərin NULL olub-olmadığını yoxlayır və NULL olarsa, əvəzində başqa bir dəyəri qaytarır.

SELECT NVL(salary, 0) AS salary FROM employees;

--Bu sorğu əmək haqqı sütununda NULL dəyərləri 0 ilə əvəz edir.


2.NVL2()

NVL2 funksiyası dəyərin NULL olub-olmamasından asılı olaraq iki fərqli dəyər qaytarır.
Birinci ifadə NULL deyilsə, ikinci ifadəni qaytarır, əgər NULL olarsa, üçüncü ifadəni qaytarır;

SELECT NVL2(salary, salary, 0) AS salary_check FROM employees;

--Bu sorğu salary NULL deyilsə, salary ni , NULL olduqda isə 0-ı qaytarır.



3.SUBSTR()

SUBSTR funksiyası sətrin müəyyən hissəsini qaytarır.

--SUBSTR(string, start_position, [length])

SELECT SUBSTR(first_name, 1, 3) AS short_name FROM employees;

--Bu sorğu FIRST_NAME sütununda ilk üç simvolu qaytarır.



4.INSTR()
 
INSTR funksiyası string sətirində xüsusi substring (alt setir) axtarır və onun mövqeyini qaytarır.

SELECT INSTR(email, '@') AS at_position FROM employees;

--Bu sorğu e-poçt sütununda @ simvolunun mövqeyini qaytarır.


5.RPAD() 

RPAD funksiyası müəyyən uzunluğa çatana qədər sətrin sonuna müəyyən simvol əlavə edir.

SELECT RPAD(first_name, 10, '*') AS padded_name FROM employees;

--Bu sorğu ilk_ad sütunundakı dəyərlərin sonuna * simvolu əlavə edərək uzunluğu 10 edir.



6.LPAD ()

LPAD funksiyası müəyyən uzunluğa çatana qədər sətrin əvvəlinə müəyyən edilmiş simvol əlavə edir.

SELECT LPAD(first_name, 10, '*') AS padded_name FROM employees;

--Bu sorğu ilk_ad sütunundakı dəyərlərin əvvəlinə * simvolu əlavə edərək uzunluğu 10 edir.



7.TRUNC()

TRUNC funksiyası nömrə və ya tarix dəyərinin müəyyən hissəsini kəsir.

SELECT TRUNC(salary,0) AS truncated_salary FROM employees;

--Bu sorğu SALARY dəyərini kəsir və tam ədədi qaytarır.


8.TRIM ()

TRIM funksiyası sətrin əvvəlindən və sonundan boşluqları və ya müəyyən edilmiş simvolları silir.

SELECT TRIM(first_name) AS trimmed_name FROM employees;

SELECT TRIM('A' FROM first_name) FROM employees

--Bu sorğu first_name dəyərindən basdaki və arxadakı boşluqları silir.


9.LTRIM()

LTRIM funksiyası sətrin əvvəlindəki müəyyən simvolları silir.

SELECT LTRIM(first_name, 'A') AS trimmed_name FROM employees;

--Bu sorğu first_name dəyərindən basindaki A simvollarını silir.

10.RTRIM()  -- LTRIM kimi isleyir sadece sondaki mueyyen simvollari silir




11.TRANSLATE()

TRANSLATE funksiyası sətirdəki müəyyən simvolları digər simvollarla əvəz edir.
Bu funksiya xüsusi simvolları bir-bir əvəz etmək üçün istifadə olunur və 
REPLACE funksiyasından fərqli olaraq birdən çox simvolu əvəz edə bilər.

TRANSLATE(string, from_chars, to_chars)


string: Dəyişdiriləcək simvol sətri.
from_chars: Əvəz ediləcək simvollar.
to_chars: sonradan evez edilecek simvollar


SELECT TRANSLATE('1-800-555-0199', '0123456789', 'ABCDEFGHIJ') AS translated_number FROM dual;


--Bu sorğu '1-800-555-0199' sətirindəki rəqəmləri hərflərə çevirir (1=A, 2=B, 3=C, ... 0=J).





12.REPLACE()

REPLACE funksiyası sətirdəki xüsusi alt sətri başqa bir alt sətirlə əvəz edir. Bu funksiya müəyyən bir alt sətri başqa bir alt sətirlə əvəz etmək üçün istifadə olunur.

REPLACE(string, search_string, replace_string)

string: Dəyişdiriləcək simvol sətri.
search_string: Əvəz ediləcək alt sətir.
replace_string: Dəyişdiriləcək alt sətri əvəz edəcək alt sətir.


SELECT REPLACE('Hello World', 'World', 'Oracle') AS replaced_string FROM dual;

--Bu sorğu "Hello World" sətirindəki "World" alt sətrini "Oracle" ilə əvəz edir və nəticəni "Hello ORacle" kimi qaytarır.



13.REVERSE: Standart Oracle SQL funksiyası olmasa da,
 istifadəçi tərəfindən müəyyən edilmiş funksiyalarla simvol sətirini tərsinə çevirə bilərik.




14.UPPER()

Upper funksiyası simvollar sətirini böyük hərflərə çevirir.

SELECT UPPER(first_name) AS upper_case FROM dual;

--Bu sorğu 'hello world' sətirini böyük hərflərə çevirir və nəticəni 'HELLO WORLD' kimi qaytarır.



15.LOWER()

LOWER funksiyası simvollar sətirini kiçik hərflərə çevirir.

SELECT LOWER('HELLO WORLD') AS lower_case FROM dual;

--bu sorgu hamisini balaca heriflerle yazdirir
hello world

16.INITCAP()

INITCAP funksiyası sətirdəki hər sözün ilk hərfini böyük hərflərə, qalan hərfləri isə kiçik hərflərə çevirir. Bu funksiya tez-tez adları və başlıqları formatlamaq üçün istifadə olunur.

SELECT INITCAP('hello world from oracle') AS initcap_string FROM dual;\

--Bu sorgu "Hello World From Oracle"

Netice:

UPPER: sətri bütün böyük hərflərə çevirir.
LOWER: Simvollar sətirini bütün kiçik hərflərə çevirir.
INITCAP: Sətirdəki hər sözün ilk hərfini böyük hərflərə, qalan hərfləri isə kiçik hərflərə çevirir.