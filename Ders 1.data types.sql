--Oracle SQL-də verilənlər bazasında saxlanılan verilənlərin növlərini təyin etmək üçün məlumat tiplərindən istifadə edilir. Budur --bəzi ümumi Oracle SQL məlumat növləri:

--Number: Ədədi dəyərləri təmsil etmək üçün istifadə olunur. Buraya tam və onluq ədədlər daxil ola bilər.
CREATE TABLE Student (
    StudentID NUMBER,
    Name VARCHAR2(50),
    Age  NUMBER
);

--VARCHAR2(n): Dəyişən simvol massivlərini (sətirlərini) saxlamaq üçün istifadə olunur. 'n' saxlanıla bilən simvolların maksimum sayını göstərir.
CREATE TABLE Student (
    StudentID NUMBER,
    First_Name VARCHAR2(50),
    Last_Name VARCHAR2(50),
    Age  NUMBER
);

--Date: Tarix və vaxt məlumatlarını saxlamaq üçün istifadə olunur.
CREATE TABLE Order (
    OrderID NUMBER,
    Dob DATE
);

--CHAR(n): Sabit simvol sətirlərini saxlamaq üçün istifadə olunur. 'n' saxlanacaq simvolların sayını təyin edir. Simvolların sayı --göstərilən dəyərə çatmazsa, boşluqlarla doldurulur.
CREATE TABLE Employee (
    EmployeeID NUMBER,
    Name CHAR(30)
);

--CLOB: Böyük mətn məlumatlarını (Character Large Object) saxlamaq üçün istifadə olunur. Bu, uzun mətn, sənədlər və ya XML --məlumatları kimi böyük məlumat hissələrini saxlamaq üçün istifadə edilə bilər.
CREATE TABLE Document (
    DocumentID NUMBER,
    Contets CLOB
);

--BLOB: Böyük ikili verilənləri (Binary Large Object) saxlamaq üçün istifadə olunur. Bu, tez-tez şəkilləri və ya digər ikili --faylları saxlamaq üçün istifadə olunur.
CREATE TABLE Picture (
    PictureID NUMBER,
    Datas BLOB
);



VARCHAR və CHAR, Oracle SQL-də simvol massivlərini saxlamaq üçün istifadə olunan iki fərqli məlumat növüdür. Funksional baxımdan oxşar olsalar da, bəzi mühüm fərqlərə malikdirlər:

Uzunluq:

VARCHAR2: Dəyişən simvol sətirlərini saxlayır. O, müəyyən edilmiş maksimum simvol sayından çoxunu ehtiva edə bilər, lakin faktiki uzunluq saxlanacaq məlumatdan asılıdır.
CHAR: Sabit uzunluqlu simvol sətirlərini saxlayır. O, müəyyən edilmiş simvol sayından daha az və ya daha çox simvol ehtiva edə bilər. Müəyyən edilmiş uzunluğa çatmaq üçün boşluqlar lazım olduqda doldurulur.


Saxlama sahəsi:

VARCHAR2: İstifadə olunan məlumatların faktiki miqdarına bərabər yaddaş yerindən istifadə edir. Bu, məlumatların həqiqi uzunluğundan asılı olaraq dəyişə bilər.
CHAR: Müəyyən edilmiş uzunluqda sahəni ehtiva edir. Beləliklə, göstərilən uzunluqdan daha az simvol olsa belə, saxlama yeri göstərilən uzunluğa əsasən ayrılır.


Performans:

VARCHAR2: Saxlama sahəsi tələbləri daha çevik olduğu üçün ümumiyyətlə daha az saxlama yerindən istifadə edir. Bununla belə, verilənlərin həqiqi uzunluğu bilinməyən və dəyişkən olduqda, bu, daha çox CPU istifadəsinə səbəb ola bilər.
CHAR: Sabit uzunluqda olduğundan həmişə eyni miqdarda saxlama yerindən istifadə edir. Buna görə də, CHAR tipli məlumatların oxunması və işlənməsi bəzən daha sürətli ola bilər.


Xülasə, VARCHAR2 dəyişən uzunluqlu simvol massivləri üçün daha çevik seçimdir, CHAR isə sabit uzunluqlu simvol massivləri üçün daha uyğun seçimdir. Ehtiyacdan asılı olaraq, hər ikisi istifadə edilə bilər, lakin saxlama yeri və performans baxımından fərqləri nəzərə almaq vacibdir.

