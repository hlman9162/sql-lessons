
select * from sales_data;
--SET Operators
/*
Set operators combine the results of two component queries into a single result.
Queries set operators are called compound queries.

*/

--Union  - All distinct rows selected by either query.
select * from sales_data where year_ between 2012 and 2014
union 
select * from sales_data where year_ between 2013 and 2014;




select first_name,last_name,salary from employees 
where salary>500
MINUS
select first_name,last_name,salary from employees
where department_id =40


A/B

--
104 -
123 -
124 -
166 -
167 -
173 -
179 -
202 -
203 -

--Union All -to retain the dublicate rows 
select * from sales_data where year_ between 2012 and 2014 
union all 
select * from sales_data where year_ between 2013 and 2014;

--Intersect -- it only returns the rows selected by all queries or data sets. 
--If a record exists in one query and not in the other, it will be omitted from the INTERSECT results.
select * from sales_data where year_ between 2012 and 2014 
intersect
select * from sales_data where year_ between 2013 and 2014;


--
104
123
124
166
167
173
179
202
203

--Minus --The MINUS operator will retrieve all records from the first dataset and then remove
--from the results all records from the second dataset.
select * from sales_data where year_ between 2012 and 2014 
minus 
select * from sales_data where year_ between 2013 and 2014;

--
113
155
161
165
178
--
select* from employees where salary between 5000 and 7000
minus 
select * from employees where salary between 5500 and 6500
minus
select * from employees where salary between 6500 and 6900;


--xeta verecek
select sale_id,customer_name from sales_data where year_ between 2012 and 2014 
union 
select sale_id from sales_data where year_ between 2013 and 2014;

--xeta verecek
select sale_id,customer_name from sales_data where year_ between 2012 and 2014 
union 
select to_number(sale_id),customer_name from sales_data where year_ between 2013 and 2014;


--xeta vermeyecek
select sale_id,customer_name from sales_data where year_ between 2012 and 2014 
union 
select customer_name,sale_id from sales_data where year_ between 2013 and 2014;


--set operations (like UNION, UNION ALL, MINUS, INTERSECT) and JOINs have a key difference:
--set operations combine rows while joins combine columns
--
select * from sales_data where year_ between 2012 and 2014 
union all 
select * from sales_data where year_ between 2013 and 2014;

select * from (select * from sales_data where year_ between 2012 and 2014)  a left join 
               (select * from sales_data where year_ between 2013 and 2014) b on a.sale_id=b.sale_id;


       
 10000 50      
 




--Найти всех клиентов, которые совершили покупки и через интернет, и в розничных магазинах:
SELECT DISTINCT customer_id FROM sales_data
WHERE sales_channel = 'Online'
INTERSECT
SELECT DISTINCT customer_id FROM sales_data
WHERE sales_channel = 'Retail';

--Найти всех клиентов, которые совершили покупки только в интернет-магазине, но не в розничных магазинах:
SELECT DISTINCT customer_id FROM sales_data
WHERE sales_channel = 'Online'
MINUS
SELECT DISTINCT customer_id FROM sales_data
WHERE sales_channel = 'Retail';


--Найти все продукты, которые были проданы и в категории 'Электроника', и в категории 'Мебель':
SELECT DISTINCT product_id FROM sales_data
WHERE product_category = 'Электроника'
INTERSECT
SELECT DISTINCT product_id FROM sales_data
WHERE product_category = 'Мебель';

--Найти всех клиентов, которые совершили покупки и в первом квартале, и в четвертом квартале 2021 года:
SELECT DISTINCT customer_id FROM sales_data
WHERE quarter = 1 AND year_ = 2021
INTERSECT
SELECT DISTINCT customer_id FROM sales_data
WHERE quarter = 4 AND year_ = 2021;


--Найти всех клиентов, которые совершили покупки с использованием определенных кодов скидок:
SELECT DISTINCT customer_id FROM sales_data
WHERE discount_code = 'DISCOUNT1'
UNION
SELECT DISTINCT customer_id FROM sales_data
WHERE discount_code = 'DISCOUNT2';

--Найти всех клиентов, которые совершили покупки с бесплатной доставкой или без применения скидки:
SELECT DISTINCT customer_id FROM sales_data
WHERE shipping_cost = 0
UNION
SELECT DISTINCT customer_id FROM sales_data
WHERE discount = 0;

select  total_spent from sales_data;