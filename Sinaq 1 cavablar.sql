--Sual 1:
-- eger iscinin 20 faiz artmis maaasi iscinin evvelki maasindan 2000 azn boyukdurse
-- o zaman iscinin tam adi, maasi ve maas ferqini gosterin

SELECT
    (FIRST_NAME || ' ' || LAST_NAME) AS FULL_NAME
    SALARY AS OLD_SALARY,
    (SALARY * 1.2) AS NEW_SALARY,
    ((SALARY * 1.2) - SALARY) AS SALARY_DIFFERENCE
FROM
    EMPLOYEES
WHERE
    (SALARY * 1.2) > 2000;

--Sual 2
--hefte sonu ise baslayan iscilerin adini soyadini maasini, tecrube ilini, ise baslama vaxtini gosterin:

SELECT
    firs_name,
    last_name,
    salary,
    (extract(year from sydate)- extract(year from hiredate)) as experience_year,
    hire_date
FROM
    employees
WHERE
    TO_CHAR(start_date, 'DY') IN ('SAT', 'SUN');


--Sual 3:
--tam adinin uzunlugu 11 e beraber olan ve employee idsi tek reqem
--olan iscilerin, tam adini, tecrube ayini, maasini gosterin:


SELECT
    first_name || ' ' || last_name AS full_name,
    FLOOR((SYSDATE-HIRE_DATE)/30) AS experience_month,
    salary
FROM
    employees
WHERE
    LENGTH(first_name || ' ' || last_name) = 11
    AND MOD(employee_id, 2) = 1;



--Sual 4:
--80-ci departamentin max ve min maasini ve onlar arasindaki ferqi gosterin
--ancaq minimum ve maksimum maas arasindaki ferq 7000 den cox olan iscilerin melumatlari gosterilsin:
---
SELECT
    MAX(salary) AS max_salary,
    MIN(salary) AS min_salary,
    (MAX(salary) - MIN(salary)) AS salary_difference
FROM
    employees
WHERE
    department_id = 80
HAVING
    (MAX(salary) - MIN(salary)) > 7000;


--Sual 5:
-- her job_idnin max minimum ve ortalama maasini gosterin

SELECT
    job_id,
    MAX(salary) AS max_salary,
    MIN(salary) AS min_salary,
    AVG(salary) AS avg_salary
FROM
    employees
GROUP BY
    job_id;



--Sual6:
-- job idsi ( it_prog, pu_man, fi_account) olan ve
-- ise baslama vaxti ayin 1 ci rubune tesaduf eden iscilerin maasini 500
-- ise balama vaxti ayin 2 ci rubune tesaduf eden iscilerin maasini ise 600 artirin:

UPDATE employees
SET salary = CASE
                WHEN TO_CHAR(hire_date, 'Q') = '1' THEN salary + 500
                WHEN TO_CHAR(hire_date, 'Q') = '2' THEN salary + 600
             END
WHERE job_id IN ('IT_PROG', 'PU_MAN', 'FI_ACCOUNT')
  AND TO_CHAR(hire_date, 'Q') IN ('1', '2');


  UPDATE employees
   SET salary= salary+500
    WHERE job_id IN ('IT_PROG', 'PU_MAN', 'FI_ACCOUNT')
  AND TO_CHAR(hire_date, 'Q')='1'

  UPDATE employees
   SET salary= salary+600
    WHERE job_id IN ('IT_PROG', 'PU_MAN', 'FI_ACCOUNT')
  AND TO_CHAR(hire_date, 'Q')='2'


UPDATE employees
SET salary = CASE
                WHEN TO_CHAR(hire_date, 'DD') <= '15' THEN salary + 500
                ELSE salary + 600
             END
WHERE job_id IN ('IT_PROG', 'PU_MAN', 'FI_ACCOUNT')
  AND TO_CHAR(hire_date, 'DD') BETWEEN '01' AND '31';


Sual 7:
--Adi E ile baslayan butun adlari Ə ilə gosterin:

SELECT
    REPLACE(first_name, 'E', 'Ə') AS modified_name
FROM
    employees
WHERE
    first_name LIKE 'E%';

Sual 8:

--nece departament oldugunu tapin ve eger her hansisa bir departamentin adi yoxdursa
--hemin bos sutuna melumat yoxdur yazilsin:


SELECT
    NVL(department_name, 'melumat yoxdur') AS department_name,
    COUNT(*) AS department_count
FROM
    departments
GROUP BY
    NVL(department_name, 'melumat yoxdur');



Sual 9:

-- nece departament oldugunu tapin ve bu defe departament idsi olmayan sutuna 0 qeyd edin:

SELECT
    NVL(department_id, 0) AS department_id,
    COUNT(*) AS department_count
FROM
    departments
GROUP BY
    NVL(department_id, 0);


Sual 10:

--employee_id si cut olan iscilerin adini soyadini ise baslama vaxtini ve tecrube ilini gosterin

SELECT
    first_name,
    last_name,
    start_date,
    experience_years
FROM
    employees
WHERE
    MOD(employee_id, 2) = 0;
