 ****************************************Tarixe aid single-row functionlar**********************************************************

 1.SYSDATE
 --movcud tarixi ve saati gosterir

 SELECT SYSDATE FROM dual;



2.TO_DATE()
--bir xarakter formatindaki deyeri tarixe cevirir

SELECT TO_DATE('2024-06-29', 'YYYY-MM-DD') FROM dual;



3.TO_CHAR()
--bir tarix formatindaki deyeri xaeakter tipine cevirir

SELECT TO_CHAR(SYSDATE, 'YYYY-MM-DD') FROM dual;



4.ADD_MONTHS()
--qeyd edilen say qeder ayi tarixe elave edir

SELECT ADD_MONTHS(SYSDATE, 6) FROM dual;


5.MONTHS_BETWEEN()
--iki tarix arasindaki ay ferqini dondurur

SELECT MONTHS_BETWEEN(SYSDATE, TO_DATE('2023-12-31', 'YYYY-MM-DD')) FROM dual;


***************************Number(eded) tipindeki deyerlere aid single-row functionlar**********************************************



1.ROUND()
--bir ededi qeyd edilen onluqa yuvarlaqlasidirir

SELECT ROUND(123.456, 2) FROM dual;


2.TRUNC()
--bir ededi qeyd edilen onluqda kesir

SELECT TRUNC(123.456, 2) FROM dual;


3.MOD()
--bölmə emeliyyatinda qalan qaligi gosterir

SELECT MOD(10, 3) FROM dual;


4.ABS()
--bir ededin mutleq deyerini gosterir(modul kimi dusune bilersen)

SELECT ABS(-123.45) FROM dual;


5.POWER()
--Bir ədədi digər ədədin gücünə qaldırır.

SELECT POWER(2, 3) FROM dual;





**STRING Tipinde yazilar ucun
LENGTH() 
--STRINGIN UZUNLUGUNU HESABLAYIR


