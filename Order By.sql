--Oracle SQL-də ORDER BY ifadəsi nəticə dəstindəki sətirləri çeşidləməyə imkan verir. 
--Bu, müəyyən bir sütun və ya sütunlar dəsti ilə artan və ya azalan qaydada çeşidləməyə imkan verir

--Defolt çeşidləmə artan qaydadadır (ASC), lakin azalan qaydada (DESC) də mövcuddur.

**Artan Sıralama (default olaraq yeni biz order by dan sonra asc yazmasaqda o artan sira ile siralanacaq):

SELECT * FROM employees
ORDER BY salary;

--Bu sorğu işçilər cədvəlindəki qeydləri əmək haqqı sütununa görə artan qaydada çeşidləyir.

**Azalan Sıralama:

SELECT * FROM employees
ORDER BY salary DESC;

--Bu sorgu, employees tablosundaki deyerleri salary sütununa göre azalan sırada sıralar.

**Birdən çox sütun üzrə siralama
--ORDER BY ifadəsi birdən çox sütundan istifadə edərək siralamaga imkan verir. 
--Bu halda, əvvəlcə birinci sütun, sonra ikinci sütun və s. siralama olaraq istifadə edərək həyata keçirilir.

SELECT * FROM employees
ORDER BY department_id, salary DESC;

--Bu sorğu əvvəlcə departament_id sütunu üzrə artan qaydada,
-- sonra eyni departament_id-ə malik olanları əmək haqqı sütunu üzrə azalan qaydada siralayir.



****************************************** Order by ve null deyerleri ***************************************************

NULLS FIRST və NULL LAST, siralama prosesində NULL dəyərlərinin necə idarə olunduğuna nəzarət etmək üçün istifadə olunur.


**NULLS FIRST

SELECT * FROM employees
ORDER BY salary NULLS FIRST;
--Bu sorğu əmək haqqı sütununa görə çeşidlənir və əvvəlcə NULL dəyərləri qaytarır.


**NULLS LAST

SELECT * FROM employees
ORDER BY salary NULLS LAST;
--Bu sorğu əmək haqqı sütununa görə çeşidlənir və NULL dəyərləri sonuncu yerləşdirir.



**NETICE:

ORDER BY nəticeni siralamq üçün istifadə olunur.
Siralama artan (ASC) və ya azalan (DESC) ola bilər.
Siralama birdən çox sütun üzrə edilə bilər.
Sifarişdə NULL dəyərlərinin yeri NULLS FIRST və ya NULL LAST ilə müəyyən edilə bilər.
