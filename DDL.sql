--DDL(Data Definition Language)
--Məlumat strukturunu dəyişdirmək, yaratmaq və idarə etmək üçün istifadə olunur. 
--DDL commands : CREATE,ALTER,TRUNCATE,DROP,PURGE,RENAME,MODIFY,FLASHBACK



1.CREATE: Yeni verilənlər bazası obyekti yaradır

CREATE TABLE employees (
    id INT PRIMARY KEY,
    name VARCHAR(100),
    position VARCHAR(100),
    salary DECIMAL(10, 2)
);



2.ALTER əmri mövcud cədvəl və ya obyekti dəyişdirmək üçün istifadə olunur.

--yeni bir sutun elave etmek
ALTER TABLE table_name
  ADD column_name datatype;


--bir sutunu silmek 
ALTER TABLE table_name
  DROP COLUMN column_name;
  

--bir sutunun data tipini deyisdirmek
ALTER TABLE table_name
  MODIFY column_name datatype;



MODIFY əmri ALTER TABLE əmrinin bir hissəsi kimi istifadə olunur və cədvəldəki sütunların xüsusiyyətlərini dəyişdirmək üçün istifadə olunur.

ALTER TABLE table_name
  MODIFY column_name datatype;

--Sutuna mehdudiyyet elave etmek

ALTER TABLE table_name
  ADD CONSTRAINT constraint_name constraint_type (column_name);

Meselen:
 
ALTER TABLE employees
  ADD CONSTRAINT emp_pk PRIMARY KEY (employee_id);



3.TRUNCATE əmri cədvəldəki bütün məlumatları tez silmək üçün istifadə olunur. Bu əməliyyatı geri qaytarmaq mümkün deyil (ROLLBACK mümkün deyil).

TRUNCATE TABLE table_name;



4.DROP əmri verilənlər bazasından cədvəl və ya başqa obyekti tamamilə silmək üçün istifadə olunur.

DROP TABLE table_name;
DROP INDEX index_name;
DROP VIEW view_name;



5.PURGE əmri zibil qutusundan obyektləri həmişəlik silmək üçün istifadə olunur.

PURGE TABLE table_name;
PURGE INDEX index_name;



6.RENAME əmri obyektin adını dəyişmək üçün istifadə olunur.

RENAME old_name TO new_name;




