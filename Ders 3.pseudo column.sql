--ROWID:
--ROWID hər bir cərgənin unikal identifikatorudur. Oracle verilənlər bazasında hər bir sətir ROWID dəyərinə malikdir və bu dəyər verilənlər bazasında sıranın fiziki yerini təmsil edir. ROWID dəyərləri cədvəl və sütun adının birləşməsidir. Nümunə olaraq ROWID dəyəri belədir: AAAAB6AABAAAAADAAAROWID, cədvəldəki xüsusi sətirə birbaşa daxil olmaq mümkündür. Bununla belə, ROWID dəyəri sıranın həyat dövrü ərzində sabit qalmır; Sətirin köçürülməsi, sıxılması və ya yerinin dəyişdirilməsi kimi əməliyyatlar ROWID dəyərini dəyişə bilər.

--ROWNUM:
--ROWNUM sorğu nəticəsində qaytarılan sətirləri nömrələmək üçün istifadə olunur. Hər bir sətrin sırası sorğunun nəticəsi əsasında müəyyən edilir və dinamik olaraq təyin edilir. Buna görə də, ROWNUM dəyəri sorğu nəticələrinə təsir edən WHERE şərtləri və digər filtrləmə əməliyyatları ilə istifadə edildikdə ehtiyatlı olmaq lazımdır: 1, 2, 3, ...ROWNUM tez-tez sorğularda çeşidləmə kimi əməliyyatlarda istifadə olunur. və ya yuxarı N cərgələri bərpa edin. Bununla belə, ROWNUM dəyəri sorğu nəticələri çeşidləndikdən sonra təyin edilir. Buna görə də, sorğu nəticələrinin çeşidlənməsini təmin etmək üçün əvvəlcə sorğudan alt sorğu daxilində istifadə etmək lazımdır.

SELECT * FROM (
    SELECT * FROM your_table ORDER BY your_column
) WHERE ROWNUM <= 5;

SELECT ROWID, column1, column2
FROM your_table
WHERE condition;


select first_name,last_name, ROWNUM FROM EMPLOYEES;