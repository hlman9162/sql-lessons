--Iki ve daha cox cedveli birlesdirerek melumatlari elde etmek ucun istifade olunur.
--Bu emeliyyatlar verilenler bazasindan lazimli melumatlari effektiv sekilde cekmeye imkan verir
--Join tipler:
--Inner Join
--Left Join
--Right Join 
--Full Join
--Cross Join 
--Self Join
--Natural Join
--Using Clause Join


--Inner Join
--Her iki cedvelde uygun gelen setirleri qaytarir

SELECT a.column1, b.column2
FROM table1 a
INNER JOIN table2 b
ON a.common_column = b.common_column;

--Burada common_column yazmagimin meqsedi her iki table uygun sutunlari goturmekdir
--sutunlarin adlari ferqli ola biler,lakin icindeki datalarin tipleri ve megzleri eyni olmalidi

--Left Join
--Sol table daki butun setileri sag table da ise sola uygun gelenleri cixarir
--Eger sag cedvelde uygun gelen setir yoxdursa NULL cixarir

SELECT a.column1, b.column2
FROM table1 a
LEFT JOIN table2 b
ON a.common_column = b.common_column;

--Right Joinde bu mentiqle isleyir

--FULL Join (Full Outer Join)
--Hem sol hemde sag table daki butun setirleri qaytarir
--Eger her iki cedvelde uygun gelen setir yoxdursa Null qaytarir

SELECT a.column1, b.column2
FROM table1 a
FULL JOIN table2 b
ON a.common_column = b.common_column;

--Cross Join 
--Birinci cedveldeki her bir setiri ikinci cedveldeki her bir setir ile birlsedirir
--Neticede Kartezian hasilat alinir

SELECT a.column1, b.column2
FROM table1 a
FULL JOIN table2 b;

--Self Join 
--Bir cedvelin ozunu ozu ile birlesdirmek ucundu

SELECT a.column1 ,b.column2
FROM table1 a,table1 b 
WHERE a.common_column = b.common_column;

--Natural Join
--Adlari ve tipleri eyni olan sutunlari avtomatik olaraq birlesdirir

SELECT * FROM table1
Natural Join table2;

--USING Clause 
--bu sert yalniz her iki table eyni adda olan sutunlar ucun kecerlidir

SELECT a.column1 ,b.column2
FROM table1 a
INNER JOIN table2 b
USING (common_column);


--SELF JOIN
--SELF JOIN cədvəli özü ilə birləşdirən birləşmə növüdür. Bu, eyni cədvəldən iki fərqli nümunə kimi istifadə etməklə edilir.
--SELF JOIN tez-tez cədvəldəki bir sıra ilə eyni cədvəldəki başqa bir sıra müqayisə etmək üçün istifadə olunur.


SELECT e1.employee_id AS Employee_ID,
       e1.first_name AS Employee_Name,
       e2.employee_id AS Manager_ID,
       e2.first_name AS Manager_Name
FROM employees e1
LEFT JOIN employees e2 ON e1.manager_id = e2.employee_id;


employees e1: Iscileri temsil eden cedvel.
employees e2: Yöneticileri temsil eden tablo.
e1.manager_id = e2.employee_id: e1 tablosundaki her çalışanın manager_id değeri, e2 tablosundaki employee_id değeri ile eşleşir.
LEFT JOIN: Birleştirme türü olarak LEFT JOIN kullanıyoruz, çünkü her çalışanın bir yöneticisi olmayabilir (örneğin, en üst düzey yönetici).

--SELF JOIN İstifadə Sahələri
İerarxik məlumat strukturlarının təhlili (məsələn, işçi və menecer münasibətləri).
Cədvəldəki xüsusi sətirləri eyni cədvəlin digər sətirləri ilə müqayisə etmək.
Zamana əsaslanan təhlilin aparılması (məsələn, məhsulda qiymət dəyişikliklərinin izlənilməsi).



emp_id     FIRST_NAME               MANAGER_ID
1           LEMAN (ISCI)                5
2           LEYLA (ISCI)                5
3           RAUF   (MUDUR)              NULL
4           FUAD   (ADMISTRATOR)        3
5           AYSEL  (QRUP REHBERI)       4

SELECT e1.employee_id AS Employee_ID,
       e1.first_name AS Employee_Name,
--ISCILER (BUTUN ISCILER)
       e2.employee_id AS Manager_ID,
       e2.first_name AS Manager_Name
FROM employees e1
LEFT JOIN employees e2 ON e1.manager_id = e2.employee_id;


