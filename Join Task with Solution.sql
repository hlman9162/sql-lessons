Task 1:
Joins
Using a LEFT JOIN, list all departments from the "departments" table and the count of employees in each department from the "employees" table.

Solution:
SELECT d.department_name, COUNT(e.employee_id) AS employee_count
FROM departments d
LEFT JOIN employees e ON d.department_id = e.department_id
GROUP BY d.department_name;


Task 2 :
Joins:
Write a query that FULL OUTER JOINs the "employees" table with the "departments" table and displays the employee's first name,
last name, and department name.


Solution:
SELECT e.first_name,e.last_name,d.department_name
FROM employees e
FULL OUTER JOIN departments d
ON e.department_id=d.department_id;


Task  3:
Joins:
Write a query that INNER JOINs the "employees" table with the "jobs" table and displays the employee's first name, last name, and job title.


Solution:
SELECT e.first_name,e.last_name,j.job_title
FROM employees e
INNER JOIN jobs j
ON e.job_id=j.job_id;



Task 4 :
Joins:
Write a query to display the first_name, last_ name, and department_name of all employees
who work in countries that have a country_name starting with 'U'.


Solution:
SELECT e.first_name,
       e.last_name,
       d.department_id,
       c.country_name
FROM employees e
INNER JOIN departments d ON e.department_id=d.department_id
INNER JOIN locations l   ON d.location_id=l.location_id
INNER JOIN countries c   ON l.country_id=c.country_id
WHERE
    c.country_name LIKE'U%';


Task 5:
Joins:
Write a query that RIGHT JOINs the "employees" table with the "departments" table and displays the employee's first name, last name, and department name.

Solution:
SELECT e.first_name,
       e.last_name,
       d.department_name
FROM employees e
RIGHT JOIN departments d
ON e.department_id=d.department_id;

