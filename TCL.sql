Transaction Control Language

Oracle SQL-də verilənlər bazası əməliyyatlarına nəzarət etmək üçün Transaction Control Language (TCL) əmrləri istifadə olunur.
Bu əmrlər tranzaksiyaların necə icra olunduğunu, davamlılığını və ya verilənlər bazasına geri qaytarılmasını müəyyən edir. 
TCL əmrlərinə COMMIT, ROLLBACK və SAVEPOINT daxildir.


*********************************************** COMMIT **************************************************************************


COMMIT əmri verilənlər bazasında müvəqqəti dəyişiklikləri daimi edir. Əməliyyat uğurla başa çatdıqda istifadə olunur. 
COMMIT-dən sonra edilən dəyişikliklər geri qaytarıla bilməz.

-- Transaction başlat
INSERT INTO employees (id, name, department) VALUES (1, 'Alice', 'HR');
INSERT INTO employees (id, name, department) VALUES (2, 'Bob', 'Finance');

-- Deyisiklikleri qalici hala getir

COMMIT;

COMMIT əmri verilənlər bazasında dəyişiklik edən bütün Data Manipulation Language (DML) əmrlərinə tətbiq oluna bilər.
Bu əmrlərə verilənlər bazasına məlumat əlavə edən, yeniləyən və ya silən əmrlər daxildir.
COMMIT əmrinin tətbiq oluna biləcəyi əsas DML əmrləri bunlardır:

INSERT INTO employees (id, name, department) VALUES (7, 'George', 'R&D');
COMMIT;

UPDATE employees SET department = 'Engineering' WHERE id = 7;
COMMIT;

DELETE FROM employees WHERE id = 7;
COMMIT;

COMMIT əmri verilənlər bazasında dəyişiklik edən bütün DML əmrləri üçün istifadə olunur, məsələn, INSERT, UPDATE, DELETE və MERGE.


********************************************** ROLLBACK *************************************************************************

ROLLBACK əmri cari əməliyyatdakı bütün dəyişiklikləri geri qaytarır. Səhv baş verdikdə və ya gözlənilməz dəyişikliklər edildikdə istifadə olunur.

-- Transaction başlad
INSERT INTO employees (id, name, department) VALUES (3, 'Charlie', 'Marketing');
INSERT INTO employees (id, name, department) VALUES (4, 'David', 'Sales');

-- Xeta bas verdi proses geri qaytarilmalidir
ROLLBACK;

Bu misalda iki yeni işçi qeydi əlavə edilir, lakin ROLLBACK əmri dəyişiklikləri heç vaxt edilməmiş kimi geri qaytarır.



********************************************** SAVEPOINT *************************************************************************

SAVEPOINT əmri əməliyyatın müəyyən nöqtəsində saxlama nöqtəsi yaradır.
Bu andan etibarən əməliyyatlar geri qaytarıla bilər.


-- Transaction başlad
INSERT INTO employees (id, name, department) VALUES (5, 'Eve', 'IT');

-- SAVEPOINT yarat
SAVEPOINT sp1;

INSERT INTO employees (id, name, department) VALUES (6, 'Frank', 'Legal');

-- İlk SAVEPOINT noqtesine geri don
ROLLBACK TO sp1;

-- Deyisikleri qalici hala getir
COMMIT;



Bu misalda İT departamentinə 'Eve' əlavə edilir və sonra SAVEPOINT sp1 yaradılır.
'Frank' Hüquq şöbəsinə əlavə edilir, lakin sonra 'Frank' əlavə edilməsi ROLLBACK TO sp1 əmri ilə ləğv edilir.
Nəhayət, COMMIT ilə 'Eve' əlavəsi qalıcı olur.


********************************************* Netice *********************************************************************


COMMIT: Tranzaksiya tamamlandıqda və uğurla başa çatdıqda istifadə olunur. Məsələn, müştəri sifarişi tamamlandıqda.

ROLLBACK: Əməliyyatda xəta baş verdikdə və ya əməliyyatı ləğv etmək istədiyiniz zaman istifadə olunur. Məsələn, funksiya xətası və ya istifadəçinin ləğvi halında.

SAVEPOINT: Böyük və mürəkkəb əməliyyatlarda müəyyən addımlarda geri dönüş nöqtələri yaratmaq üçün istifadə olunur. Məsələn, toplu məlumat yeniləmələri edərkən hər addımda yoxlama nöqtələri yaratmaq


