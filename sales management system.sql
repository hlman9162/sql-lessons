Layihə : Satış İdarəetmə Sistemi
Məqsəd: Müştərilər, məhsullar və sifarişlər arasındakı əlaqələri anlamaq.

Cədvəllər:

customers
products
orders

Məhdudiyyətlər və Əlaqələr:

Hər bir müştərinin unikal müştəri nömrəsi olmalıdır (PRIMARY KEY).
Hər bir məhsulun unikal məhsul nömrəsi olmalıdır (PRIMARY KEY).
Sifarişlər cədvəlində hər bir sifariş müştəri və məhsulla əlaqələndirilməlidir (Foreıgn KEY).