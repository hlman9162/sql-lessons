
Məqsəd: Tələbələr, müəllimlər və kurslar arasındakı münasibətləri başa düşmək.

Table'lar:
--students
--teachers
--courses
--enrollments(qeydiyyat cedveli)

Məhdudiyyətlər və Əlaqələr:

Hər bir tələbənin unikal tələbə nömrəsi olmalıdır (PRIMARY KEY).
Hər bir müəllimin unikal müəllim nömrəsi olmalıdır (PRIMARY KEY).
Hər bir kursun unikal kurs nömrəsi olmalıdır (PRIMARY KEY).
Qeydiyyat cədvəlində hər bir tələbə yalnız bir kursa (UNİKAL) yazıla bilər.
Qeydiyyat cədvəlində tələbə ilə kurs arasında FOREIGN KEY əlaqə olmalıdır.


TASK: Demeli,qeyd etdiyim table'lari yaradiriq,qeyd etdiyim mehdudiyyetlere (constraintlere) esasen yaratmaq lazimdi
Ipucu :Esas elaqeler enrollments (qeydiyyat) cedvelinde olmalidir

--çalışsan yazarsan)