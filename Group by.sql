--Oracle SQL-də GROUP BY və qrup funksiyaları (həmçinin aggregate funksiyalar kimi tanınır) 
--çoxlu sətirləri qruplaşdırmaq və həmin qruplar üzərində ümumi əməliyyatları yerinə yetirmək üçün istifadə olunur.
-- GROUP BY və bəzi mühüm qrup funksiyaları haqqında geniş izahat buradadır:

GROUP BY clause:

--Məlumat dəstini bir və ya bir neçə sütunla qruplaşdırmaq üçün istifadə olunur. 
--Qrup funksiyalarından GROUP BY ilə birlikdə istifadə etməklə hər qrup üçün ümumi nəticələr əldə etmək olar.

SELECT department_id, COUNT(*)
FROM employees
GROUP BY department_id;

--Bu sorğu işçilər cədvəlində hər bir şöbədə işçilərin sayını qaytarır


*******************************************  GROUP FUNCTIONS  ***************************************************************


1.COUNT()

--sətir sayını döndürür (rows)

SELECT department_id, COUNT(*) AS employee_count
FROM employees
GROUP BY department_id;


2.SUM()

--sutundaki deyerleri cemleyir

SELECT department_id, SUM(salary) AS total_salary
FROM employees
GROUP BY department_id;

--her bir departmentin maaslarini cemleyir


3.AVG()

--butun deyerlerin ortalamasini dondurur

SELECT department_id, AVG(salary) AS average_salary
FROM employees
GROUP BY department_id;

--her bir departmentin ortalama maasini hesablayir


4.MAX()

--sutundaki en yuksek deyeri dondurur

SELECT department_id, MAX(salary) AS highest_salary
FROM employees
GROUP BY department_id;


5.MIN()

--sutundaki en kicik deyeri donderir

SELECT department_id, MIN(salary) AS lowest_salary
FROM employees
GROUP BY department_id;


************************************************  HAVING  *********************************************************************


--HAVING bəndi GROUP BY ilə birlikdə istifadə edilən filtrləmə bəndidir.
-- HAVING qruplar üzrə şərtləri müəyyən etmək üçün istifadə olunur.


SELECT department_id, COUNT(*) AS employee_count
FROM employees
GROUP BY department_id
HAVING COUNT(*) > 10;




--Bu sorğu 10-dan çox işçisi olan şöbələri qaytarır.


--DAHA QARISIQ NUMUNE


SELECT department_id, 
       COUNT(*) AS employee_count, 
       AVG(salary) AS average_salary, 
       MAX(salary) AS highest_salary, 
       MIN(salary) AS lowest_salary
FROM employees
GROUP BY department_id
HAVING AVG(salary) > 5000;


--Bu sorğu orta əmək haqqı 5000-dən çox olan şöbələri və həmin şöbələrdə çalışan işçilərin sayını,
-- orta əmək haqqını, ən yüksək maaşı və ən aşağı əmək haqqını qaytarır.


********************************************** NETICE ***********************************************************************


GROUP BY clause: Məlumatları müəyyən sütunlara görə qruplaşdırır.
Qrup Funksiyaları: Hər qrup üçün AGGREGATE hesablamaları yerinə yetirir (COUNT, SUM, AVG, MAX, MIN).
HAVING bəndi: Qruplara filtrlər tətbiq etmək üçün istifadə olunur (çox vaxt WHERE ilə qarışdırılır, lakin HAVING qruplar üzərində işləyir).


